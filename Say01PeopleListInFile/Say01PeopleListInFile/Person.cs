﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Say01PeopleListInFile
{
    class Person
    {
        private string _name;
        private int _age;
        private string _city;

        public Person(string name, int age, string city)
        {
            Name = name;
            Age = age;
            City = city;
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (!Regex.IsMatch(value, @"^[^;]{2,100}$"))
                {
                    throw new ArgumentException("name nust be 2-100 characters and not contain semicolons");
                }
                _name = value;
            }
        }

        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new ArgumentException("age must be between 0-150");
                }
                _age = value;
            }
        }
        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if (!Regex.IsMatch(value, @"^[^;]{2,100}$"))
                {
                    throw new ArgumentException("city must be 2-50 charcters and not contain ;");
                }
                _city = value;
            }
        }
        public override string ToString()
        {
            return $"{Name} is {Age} from {City}";
        }
    }
}
