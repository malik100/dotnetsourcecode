﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MidtermEntityFramework
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                Globals.ctx = new PersonPassportDBContext();
                ReloadRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }
            
        }

        private void MIAddPerson_Click(object sender, RoutedEventArgs e)  //Add person from menu item
        {
            try
            {
                AddPersonDialog addPersonDlg = new AddPersonDialog();
                addPersonDlg.Owner = this;
                if (addPersonDlg.ShowDialog() == true)
                {
                    Person newPerson = addPersonDlg.ReturnCurrPerson;
                    Globals.ctx.People.Add(newPerson);
                    Globals.ctx.SaveChanges();
                }
                ReloadRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            
        }
        public void ReloadRecords()
        {
            try
            {
                lvPeople.ItemsSource = Globals.ctx.People.ToList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void lvPeople_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Person selectedPerson = (Person)lvPeople.SelectedItem;
                if (selectedPerson == null) { return; }
                PassportDialog passportDlg = new PassportDialog(selectedPerson);
                passportDlg.Owner = this;
                passportDlg.ShowDialog();
                ReloadRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void MIExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
