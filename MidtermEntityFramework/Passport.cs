﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidtermEntityFramework
{
    public class Passport
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(8)]
        public string PassportNo { get; set; }
        [Required]
        public byte[] Photo { get; set; }
        [Required]
        public virtual Person PassportOwner { get; set; }  // 1 to 1 relationship
    }
}
