﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MidtermEntityFramework
{
    /// <summary>
    /// Interaction logic for AddPersonDialog.xaml
    /// </summary>
    public partial class AddPersonDialog : Window
    {
        private Person _currPerson;
        public Person ReturnCurrPerson  //Read only property to return currPerson to main windoe
        {
            get
            {
                return _currPerson;
            }
        }
        public AddPersonDialog()
        {
            InitializeComponent();
        }

        private void btAddPerson_Click(object sender, RoutedEventArgs e)
        {
            if (!VerifyPerson(tbPersonName.Text, tbPersonAge.Text)) { return; } //verify inputs
            int.TryParse(tbPersonAge.Text, out int age); //Already verified as a parsable value by above method no ex
            _currPerson = new Person { Name = tbPersonName.Text, Age = age };
            DialogResult = true;
        }

        public bool VerifyPerson(string name, string age)
        {
            List<string> errorList = new List<string>();
            if (!int.TryParse(tbPersonAge.Text, out int ageInt))    //verifies value is parsable int
            {
                MessageBox.Show("Please enter a nuemeric value for age", "Input Error", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            if (!Regex.IsMatch(name, @"^[a-zA-Z ]{2,50}$"))          //verifies name
            {
                errorList.Add("Please enter a name between 2-50 alpahbetic characters only");
            }
            if (ageInt < 1 || ageInt > 150)                         //verifies age
            {
                errorList.Add("Please enter an age between 1 and 150 years");
            }
            if (errorList.Count > 0)                                //collects & displays errors
            {
                MessageBox.Show("Please fix following issues before adding person\n\n-" + string.Join("\n-", errorList), "Input Error(s)", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            return true;
        }
    }
}
