﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MidtermEntityFramework
{
    /// <summary>
    /// Interaction logic for PassportDialog.xaml
    /// </summary>
    public partial class PassportDialog : Window
    {
        private Person currOwner;
        private Passport currPassport;
        private byte[] currPhoto;
        public PassportDialog(Person passedPerson = null)
        {
            try
            {
                InitializeComponent();
                currOwner = passedPerson;
                currPassport = currOwner == null ? null : currOwner.Passport; //Null or previous passport loaded for add/update
                lblPassOwner.Content = currOwner.Name;
                if (currPassport == null)  //New passport
                {
                    tbImage.Visibility = Visibility.Visible;
                    imgPreview.Visibility = Visibility.Hidden;
                    btAddUpdatePass.Content = "Add";
                }
                else                    //Existing passport
                {
                    tbImage.Visibility = Visibility.Hidden;     //toggle text/picture visibility
                    imgPreview.Visibility = Visibility.Visible;
                    btAddUpdatePass.Content = "Update";
                    tbPassportNo.Text = currPassport.PassportNo;    //load correct info to GUI
                    currPhoto = currOwner.Passport.Photo;
                    imgPreview.Source = Utils.ByteArrayToBitmapImage(currPhoto);  //System ex possible

                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }


        }

        private void btAddUpdatePass_Click(object sender, RoutedEventArgs e)
        {
            if (!VerifyPassport(tbPassportNo.Text, currPhoto)) { return; }
            try
            {
                if (currPassport == null)           //Add new passport
                {
                    Passport newPass = new Passport { PassportNo = tbPassportNo.Text, Photo = currPhoto, PassportOwner = currOwner };
                    currOwner.Passport = newPass;
                    Globals.ctx.Passport.Add(newPass);
                }
                else                             //Edit existing passport
                {
                    currPassport.PassportNo = tbPassportNo.Text;
                    currPassport.Photo = currPhoto;
                }
                Globals.ctx.SaveChanges();
                DialogResult = true;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            
        }

        private bool VerifyPassport(string name, byte[] photo)
        {
            List<string> errorList = new List<string>();
            if (!Regex.IsMatch(name, @"^[A-Z]{2}[0-9]{6}$"))    //verify passportNo.
            {
                errorList.Add("Passport number is folowing format (AB123456)");
            }
            if (photo == null)                                  //verify photo is not null
            {
                errorList.Add("Please select a photo");
            }
            if (errorList.Count > 0)                            //collects and displays errors
            {
                MessageBox.Show("Please fix following issues before adding person\n\n-" + string.Join("\n-", errorList), "Input Error(s)", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            return true;
        }

        private void imgButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png|All Files (*.*)|*.*";
            if (dlg.ShowDialog() == true)
            {
                try
                {
                    currPhoto = File.ReadAllBytes(dlg.FileName);                  //IO exception possible
                    BitmapImage bitmap = Utils.ByteArrayToBitmapImage(currPhoto); // system exception possible
                    imgPreview.Source = bitmap;
                    imgPreview.Visibility = Visibility.Visible;
                    tbImage.Visibility = Visibility.Hidden;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    MessageBox.Show(this, ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
    }
}
