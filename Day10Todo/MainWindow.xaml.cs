﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10Todo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private TodoContext ctx;
        private static Todo currTodo = null;

        public MainWindow()
        {
            InitializeComponent();
            ctx = new TodoContext();
            lvTodo.ItemsSource = (from t in ctx.TodosDb select t).ToList<Todo>();
        }

        private void MIAdd_Click(object sender, RoutedEventArgs e)
        {
            currTodo = null; // clear previous todo if selected
            AddEditAction(currTodo);
        }

        public void AddEditAction(Todo selectedTodo)
        {
            AddEditDialog dlg = new AddEditDialog(selectedTodo, ctx);
            dlg.Owner = this;
            if (dlg.ShowDialog() == true)
            {
                lvTodo.ItemsSource = (from t in ctx.TodosDb select t).ToList<Todo>();  //refresh list
            }
            lvTodo.SelectedIndex = -1;  //deselect items and make current todo null
        }

        private void lvTodo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currTodo = lvTodo.SelectedItem as Todo; //update current used todo
        }

        private void lvTodo_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lvTodo.SelectedIndex == -1) { return; }
            AddEditAction(currTodo);
        }
    }
}
