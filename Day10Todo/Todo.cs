﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day10Todo
{
    public class Todo
    {
        int _Id;
        string _task;
        DateTime _dueDate;
        StatusEnum _status;

        public enum StatusEnum
        {
            Pending = 1, Done = 2, Delegated = 3
        }
        private Todo()
        {
        }
        public Todo( string task, DateTime dueDate, StatusEnum status)
        {
            //Id = id;
            Task = task;
            DueDate = dueDate;
            Status = status;
        }

        public string Task
        {
            get { return _task; }
            set
            {
                if (!Regex.IsMatch(value, @"^[a-zA-Z0-9 _%(),\/\\\.-]{1,100}$"))
                {
                    throw new WrongDataEx("Task must be 1-100 letters with %_-(),./\\ characters only");
                }
                _task = value;
            }

        }
        public int Id
        {
            get { return _Id; }
            set
            {
                if (value < 1)
                {
                    throw new WrongDataEx("Id can't be a negative number or zero");
                }
                _Id = value;
            }
        }

        public DateTime DueDate
        {
            get { return _dueDate; }
            set
            {
                if (value < DateTime.Now)
                {
                    throw new WrongDataEx("Due date can not be in the past");
                }
                _dueDate = value;
            }
        }
        public StatusEnum Status
        {
            get { return _status; }
            set
            {
                _status = value;
            }
        }
        // ReadOnly DueDate------------------------------------
        public string FormatedDueDate
        {
            get
            {
                return _dueDate.ToString("d");
            }
        }
        
        public override string ToString()
        {
            return $"{Id}: {Task} by {FormatedDueDate} status: {Status}";
        }

    }

    //----------CUSTOM EXCEPTION CLASS----------------------------------------------------
    public class WrongDataEx : Exception

    {
        public WrongDataEx(string msg) : base(msg) { }
    }
}
