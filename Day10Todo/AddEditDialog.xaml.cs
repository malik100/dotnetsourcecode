﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day10Todo
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {

        private Todo _workingTodo;
        private TodoContext _workingContext;

        public AddEditDialog(Todo todo = null, TodoContext context = null)
        {
            _workingTodo = todo;
            _workingContext = context;
            InitializeComponent();
            comboStatus.ItemsSource = Enum.GetValues(typeof(Todo.StatusEnum)).Cast<Todo.StatusEnum>();
            if (_workingTodo == null)
            {
                btAddEdit.Content = "Add Todo";    //change button content and disable delete button
                btDeleteTodo.IsEnabled = false;
            }
            else
            {
                btAddEdit.Content = "Modify Todo";  //load selected todo properties to inputs and enable delete button
                btDeleteTodo.IsEnabled = true;
                lblId.Content = todo.Id.ToString();
                tbTask.Text = todo.Task;
                dpDueDate.SelectedDate = todo.DueDate;
                comboStatus.SelectedItem = todo.Status;
            }
        }

        private void btAddEdit_Click(object sender, RoutedEventArgs e)
        {
            AddEditTodo(_workingTodo, _workingContext);
        }
        public void AddEditTodo(Todo workingTodo, TodoContext currContext)
        {
            try
            {
                if (dpDueDate.SelectedDate == null)  //date must be selected
                {
                    throw new WrongDataEx("Please select a date");
                }
                string task = tbTask.Text;       //adding selected todo info to inputs
                DateTime dueDate = (DateTime)dpDueDate.SelectedDate;
                Todo.StatusEnum status = (Todo.StatusEnum)comboStatus.SelectedItem;
                if (_workingTodo == null)   //new todo added
                {
                    Todo todo = new Todo(task, dueDate, status);
                    currContext.TodosDb.Add(todo);
                }
                else
                {
                    _workingTodo.Task = task;        //existing todo updated
                    _workingTodo.DueDate = dueDate;
                    _workingTodo.Status = status;
                    currContext.SaveChanges();
                }
                currContext.SaveChanges();   //save changes and exit
                _workingTodo = null;
                DialogResult = true;
            }
            catch (WrongDataEx ex)
            {
                MessageBox.Show(this, "Error: " + ex.Message, "Error adding todo", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btDeleteTodo_Click(object sender, RoutedEventArgs e)
        {
            if (_workingTodo == null) { return; } //exit if nothing selected
            if(MessageBox.Show(this, "Are you sure you want to delete the following item?\n" + _workingTodo, "Error adding todo", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
            {
                _workingContext.TodosDb.Remove(_workingTodo);
                _workingContext.SaveChanges();
                _workingTodo = null;  //clear current todo
                DialogResult = true;  //exit to main window
            }
            else
            {
                return;
            }
            
        }
    }
}
