﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
namespace Day09FriendsCustomDlg
{
    [Serializable()]
    public class Todo : ISerializable
    {
        string _task;
        int _difficulty;
        DateTime _dueDate;
        EStatus _status;

        public Todo(string task, int difficulty, DateTime dueDate, EStatus status)
        {
            Task = task;
            Difficulty = difficulty;
            DueDate = dueDate;
            Status = status;
        }
        public Todo(string line)
        {
            try
            {
                string[] data = line.Split(';');
                if (data.Length != 4)
                {
                    throw new WrongDataEx("Data from file must have 4 portions separated by ';'");
                }
                Task = data[0];
                if (!int.TryParse(data[1], out int difficulty))
                {
                    throw new WrongDataEx("Could not parse 'Difficulty' integer from file");
                }
                Difficulty = difficulty;
                DueDate = DateTime.ParseExact(data[2], "yyyy-MM-dd", CultureInfo.InvariantCulture);
                if (!Enum.TryParse(data[3], out EStatus stat))
                {
                    throw new WrongDataEx("Enum could not be parsed from file");
                }
                Status = stat;
            }
            catch (FormatException ex)
            {
                throw new WrongDataEx(ex.Message);
            }


        }

        public string Task
        {
            get { return _task; }
            set
            {
                if (!Regex.IsMatch(value, @"^[a-zA-Z0-9 _%(),\/\\\.-]{1,100}$"))
                {
                    throw new WrongDataEx("Task must be 1-100 letters with %_-(),./\\ characters only");
                }
                _task = value;
            }

        }
        public int Difficulty
        {
            get { return _difficulty; }
            set
            {
                if (value < 1 || value > 5)
                {
                    throw new WrongDataEx("Difficulty must be between 1-5");
                }
                _difficulty = value;
            }
        }

        public DateTime DueDate
        {
            get { return _dueDate; }
            set
            {
                if (value < DateTime.Now)
                {
                    throw new WrongDataEx("Due date can not be in the past");
                }
                _dueDate = value;
            }
        }
        // ReadOnly DueDate------------------------------------
        public string FormatedDueDate
        {
            get
            {
                return _dueDate.ToString("d");
            }
        }
        public EStatus Status
        {
            get { return _status; }
            set
            {
                _status = value;
            }
        }
        public override string ToString()
        {
            return $"{Task} by {DueDate} (Difficulty: {Difficulty}, {Status})";
        }
        public string ToDataString()
        {
            return $"{Task};{DueDate};{Difficulty};{Status}";
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Task", Task, typeof(string));
            info.AddValue("Difficulty", Difficulty, typeof(int));
            info.AddValue("DueDate", DueDate, typeof(DateTime));
            info.AddValue("Status", Status, typeof(EStatus));
        }

        public Todo(SerializationInfo info, StreamingContext context)
        {
            Task = (string)info.GetValue("Task", typeof(string));
            Difficulty = (int)info.GetValue("Difficulty", typeof(int));
            DueDate = (DateTime)info.GetValue("DueDate", typeof(DateTime));
            Status = (EStatus)info.GetValue("Status", typeof(EStatus));
        }

    }
    // Enum Status------------------------------------------------------------------------
    public enum EStatus
    {
        Pending, Done, Delegated
    }
    //----------CUSTOM EXCEPTION CLASS----------------------------------------------------
    public class WrongDataEx : Exception

    {
        public WrongDataEx(string msg) : base(msg) { }
    }
}
