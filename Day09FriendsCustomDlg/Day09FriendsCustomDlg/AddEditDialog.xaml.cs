﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day09FriendsCustomDlg
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    /// 

    public partial class AddEditDialog : Window
    {

        private Todo _currTodo;
        public Todo ReturnCurrTodo
        {
            get
            {
                return _currTodo;
            }
        }
        public AddEditDialog(Todo todo = null)
        {
            InitializeComponent();
            comboStatus.ItemsSource = Enum.GetValues(typeof(EStatus)).Cast<EStatus>();
            if (todo == null)
            {
                btSave.Content = "Add";
            }
            else
            {
                btSave.Content = "Update";
                tbTask.Text = todo.Task;
                sliderDifficulty.Value = todo.Difficulty;
                dpDueDate.SelectedDate = todo.DueDate;
                comboStatus.SelectedItem = todo.Status;
            }

        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            AddEditAction();
        }
        private void AddEditAction()
        {
            try
            {
                List<string> errorList = new List<string>();
                if (dpDueDate.SelectedDate == null || dpDueDate.SelectedDate < DateTime.Now)
                {
                    errorList.Add("Date field can not be empty and a past date cannot be selected");
                }
                if (!Regex.IsMatch(tbTask.Text, @"^[a-zA-Z0-9 _%(),\/\\\.-]{1,100}$"))
                {
                    errorList.Add("Task must be 1-100 letters with %_-(),./\\ characters only");
                }
                if (errorList.Count != 0)
                {
                    MessageBox.Show(this, "Please fix the following issues before adding Todo:\n\n-" + String.Join<string>("\n-", errorList), "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                string task = tbTask.Text;
                int difficulty = (int)sliderDifficulty.Value;
                DateTime dueDate = (DateTime)dpDueDate.SelectedDate;
                EStatus status = (EStatus)comboStatus.SelectedItem;
                DialogResult = true;
                _currTodo = new Todo(task, difficulty, dueDate, status);
            }
            catch (WrongDataEx ex)
            {
                MessageBox.Show(this, "Could not save Todo: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}
