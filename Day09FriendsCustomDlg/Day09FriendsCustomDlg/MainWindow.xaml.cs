﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day09FriendsCustomDlg
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        private List<Todo> TodoList = new List<Todo>();
        const string InputFile = @"../../input.txt";
        const string OutputFile = @"../../output.txt";
        public MainWindow()
        {
            InitializeComponent();
            lvTodo.ItemsSource = TodoList;
            LoadDataFromFile();
            lvTodo.Items.Refresh();
        }

        private void miEditAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialog dlg = new AddEditDialog();
            dlg.Owner = this;
            if (dlg.ShowDialog() == true)
            {
                TodoList.Add(dlg.ReturnCurrTodo);
                lvTodo.Items.Refresh();
            }
            lvTodo.SelectedIndex = -1;
        }

        private void lvFriends_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditExisting();
            lvTodo.SelectedIndex = -1;
        }

        private void contextMenuAddEdit_Click(object sender, RoutedEventArgs e)
        {
            EditExisting();
            lvTodo.SelectedIndex = -1;
        }

        private void EditExisting()
        {
            Todo item = (Todo)lvTodo.SelectedItem;
            if (item == null) { return; }
            AddEditDialog dlg = new AddEditDialog(item);
            dlg.Owner = this;
            if (dlg.ShowDialog() == true)
            {
                int index = TodoList.IndexOf(item);
                TodoList[index] = dlg.ReturnCurrTodo;
                lvTodo.Items.Refresh();
            }
        }

        private void contextMiDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvTodo.SelectedIndex == -1)
            {
                return;
            }
            if (MessageBox.Show(this, "Are you sure you want to delete the following item:\n\n" + lvTodo.SelectedItem, "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                TodoList.RemoveAt(lvTodo.SelectedIndex);
                lvTodo.Items.Refresh();
            }
        }

        private void miExport_Click(object sender, RoutedEventArgs e)
        {
            var selItems = lvTodo.SelectedItems.Cast<Todo>().ToList();
            if (selItems.Count == 0)
            {
                MessageBox.Show(this, "Please select some items first", "Export error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.FilterIndex = 1;
            // saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog.ShowDialog() == true)
            {
                string fileName = saveFileDialog.FileName;
                SaveDataToFile(fileName, selItems);
            }
            lvTodo.SelectedIndex = -1;
        }

        public void SaveDataToFile(string fileName, List<Todo> TodosToSave)
        {
            try
            {
                List<string> linesList = new List<string>();
                foreach (Todo t in TodosToSave)
                {
                    linesList.Add(t.ToDataString());
                }
                File.WriteAllLines(fileName, linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show(this, ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        public void LoadDataFromFile()
        {
            try
            {
                if (!File.Exists(InputFile))
                {
                    Console.WriteLine("file to load from not found>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                    return;
                }
                string[] linesArray = File.ReadAllLines(InputFile);
                List<string> errorList = new List<string>();
                foreach (string line in linesArray)
                {
                    try
                    {
                        TodoList.Add(new Todo(line));
                    }
                    catch (Exception ex) when (ex is WrongDataEx || ex is FormatException)
                    {
                        errorList.Add(line + "\nREASON: " + ex.Message + "\n");
                    }
                }
                if (errorList.Count != 0)
                {
                    string errors = "";
                    foreach (var item in errorList)
                    {
                        errors += item + "\n";
                    }
                    MessageBox.Show("The following lines could not be added due to errors\n\n" + errors, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult exitBox = MessageBox.Show(this, "You are about to close program. Save all changes made during session?", "Save?", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);
            if (exitBox == MessageBoxResult.Yes)
            {
                SaveDataToFile(OutputFile, TodoList);
            }else if (exitBox == MessageBoxResult.Cancel)
            {
                e.Cancel = true;
            }

        }

        private void miImport_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
