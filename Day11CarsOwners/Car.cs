﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11CarsOwners
{
    public class Car
    {
        public int Id { get; set; }
        public string MakeModel { get; set; }
        //[ForeignKey("Owner")]
        //public int OwnerId { get; set; }
        public Owner Owner { get; set; }


        public override string ToString()
        {
            return $"ID: {Id} Make/Model: {MakeModel} Owner{Owner}";
        }
    }
}
