using System;
using System.Data.Entity;
using System.IO;
using System.Linq;

namespace Day11CarsOwners
{
    public class VehiclesDBContext : DbContext
    {
        // Your context has been configured to use a 'VehiclesDBModel' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'Day11CarsOwners.VehiclesDBModel' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'VehiclesDBModel' 
        // connection string in the application configuration file.
        const string DbName = "CarOwnerDatabase.mdf";
        static string DbPath = Path.Combine(Environment.CurrentDirectory, DbName);
        public VehiclesDBContext() : base($@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={DbPath};Integrated Security=True;Connect Timeout=30") { }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<Car> Cars { get; set; }
        public virtual DbSet<Owner> Owners { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}