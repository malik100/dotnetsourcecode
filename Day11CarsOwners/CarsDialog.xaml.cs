﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day11CarsOwners
{
    /// <summary>
    /// Interaction logic for CarsDialog.xaml
    /// </summary>
    public partial class CarsDialog : Window
    {
        private Owner currCarOwner;
        private Car currCar;
        public CarsDialog(Owner passedOwner)
        {
            InitializeComponent();
            lblCarsDlgOName.Content = passedOwner.Name;
            currCarOwner = passedOwner;
            //IEnumerable<Car> retrievedCars = (from c in currContext.Cars where c.Owner.Id == currCarOwner.Id select c);
            //lvCars.ItemsSource = (from c in Globals.ctx.Cars where c.Owner.Id == currCarOwner.Id select c).ToList<Car>();
            ReloadCarRecordsForOwner();
        }

        private void btCarsDlgAdd_Click(object sender, RoutedEventArgs e)
        {
            CarDlgAddEditAction();
        }
        private void btCarsDlgUpdate_Click(object sender, RoutedEventArgs e)
        {
            CarDlgAddEditAction();
        }


        private void lvCars_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(lvCars.SelectedIndex == -1)
            {
                currCar = null;
                tbCarsDlgMakeModel.Text = "";
                lblCarsDlgCarId.Content = "...";
                btCarsDlgAdd.IsEnabled = true;
                btCarsDlgUpdate.IsEnabled = false;
                btCarsDlgDelete.IsEnabled = false;
            }
            else
            {
                currCar = (Car)lvCars.SelectedItem;
                tbCarsDlgMakeModel.Text = currCar.MakeModel;
                lblCarsDlgCarId.Content = currCar.Id;
                btCarsDlgAdd.IsEnabled = false;
                btCarsDlgUpdate.IsEnabled = true;
                btCarsDlgDelete.IsEnabled = true;
            }
        }
        private void CarDlgAddEditAction()
        {
            string makeModel = tbCarsDlgMakeModel.Text;
            try
            {
                if (currCar == null)
                {
                    Car newCar = new Car { MakeModel = makeModel, Owner = currCarOwner };
                    Globals.ctx.Cars.Add(newCar);
                }
                else
                {
                    currCar.MakeModel = makeModel;
                }
                Globals.ctx.SaveChanges();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            //IEnumerable<Car> retrievedCars = (from o in currContext.Cars where o.Owner.Id == currCarOwner.Id select o);
            //lvCars.ItemsSource = (from o in Globals.ctx.Cars where o.Owner.Id == currCarOwner.Id select o).ToList<Car>();
            ReloadCarRecordsForOwner();
            tbCarsDlgMakeModel.Text = "";
            lvCars.SelectedIndex = -1;
        }

        private void btCarsDlgDelete_Click(object sender, RoutedEventArgs e)
        {
            if (currCar == null) { return; }
            if (MessageBox.Show("Permanantley delete following record?:\n\n" + currCar.MakeModel, "Delete record?", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {
                return;
            }
            try
            {
                Globals.ctx.Cars.Remove(currCar);
                Globals.ctx.SaveChanges();
                tbCarsDlgMakeModel.Text = "";
                lvCars.SelectedIndex = -1;
                ReloadCarRecordsForOwner();
                //lvCars.ItemsSource = (from o in Globals.ctx.Cars where o.Owner.Id == currCarOwner.Id select o).ToList<Car>();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

        }

        private void btCarsDlgDone_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void ReloadCarRecordsForOwner()
        {
            try
            {
                List<Car> carList = currCarOwner.CarsInGarage.ToList<Car>();
                lvCars.ItemsSource = carList;
                Utils.AutoResizeColumns(lvCars);
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            
        }
    }
}
