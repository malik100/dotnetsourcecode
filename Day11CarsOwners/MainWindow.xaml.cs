﻿using CsvHelper;
using CsvHelper.Configuration;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day11CarsOwners
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Owner currOwner;
        private byte[] currOwnerImage;
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                Globals.ctx = new VehiclesDBContext();
                //lvOwners.ItemsSource = (from o in Globals.ctx.Owners select o).ToList<Owner>();
                ReloadRecords();
                //IList<Car> studList = ctx.Cars.ToList<Car>();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }
        }
        //SELECTION CHANGED UPDATES --------------------------------------------------------------------------
        private void lvOwners_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvOwners.SelectedIndex == -1)
            {
                RefreshGUI();
                return;
            }
            try
            {
                currOwner = (Owner)lvOwners.SelectedItem;
                lblOwnerId.Content = ((Owner)lvOwners.SelectedItem).Id;
                tbOwnerName.Text = ((Owner)lvOwners.SelectedItem).Name;
                currOwnerImage = currOwner.Photo;
                btOwnerAdd.IsEnabled = false;
                btOwnerDelete.IsEnabled = true;
                btOwnerUpdate.IsEnabled = true;
                btOwnerManageCars.IsEnabled = true;
                //backup refresh to properly display/hide picture/text
                if (currOwnerImage != null)
                {
                    imgSelected.Source = Utils.ByteArrayToBitmapImage(currOwnerImage);
                    imgSelected.Visibility = Visibility.Visible;
                    tbImage.Visibility = Visibility.Hidden;
                }
                else
                {
                    imgSelected.Visibility = Visibility.Hidden;
                    tbImage.Visibility = Visibility.Visible;
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Loading image failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        // ADD UPDATE DELETE METHODS-----------------------------------------------------------------------------
        private void btOwnerAdd_Click(object sender, RoutedEventArgs e)
        {
            AddUpdateAction();
        }

        private void btOwnerUpdate_Click(object sender, RoutedEventArgs e)
        {
            AddUpdateAction();
        }

        private void btOwnerDelete_Click(object sender, RoutedEventArgs e)
        {
            if (currOwner == null) { return; }
            if (MessageBox.Show("Permanantley delete following record?:\n\n" + currOwner.Name, "Delete record?", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {
                return;
            }
            Globals.ctx.Owners.Remove(currOwner);
            Globals.ctx.SaveChanges();
            lvOwners.ItemsSource = (from o in Globals.ctx.Owners select o).ToList<Owner>();
            RefreshGUI();
        }
        private void RefreshGUI()
        {
            currOwner = null;
            currOwnerImage = null;
            btOwnerDelete.IsEnabled = false;
            btOwnerAdd.IsEnabled = true;
            btOwnerUpdate.IsEnabled = false;
            btOwnerManageCars.IsEnabled = false;
            lblOwnerId.Content = "-";
            tbOwnerName.Text = "";
            imgSelected.Visibility = Visibility.Hidden;
            tbImage.Visibility = Visibility.Visible;
            lvOwners.SelectedIndex = -1;
        }
        public bool VerifyOwnerInputs()
        {
            if(!Regex.IsMatch(tbOwnerName.Text, @"^[a-zA-z ]{2,50}$"))
            {
                MessageBox.Show(this, "Owner name must be between 2-50 alphabetic charcters", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            return true;
        }

        
        public void AddUpdateAction()
        {
            if (!VerifyOwnerInputs()) { return; }
            try
            {
                if (currOwner == null)
                {
                    string name = tbOwnerName.Text;
                    Owner owner = new Owner { Name = name, CarsInGarage = new List<Car>(), Photo = currOwnerImage };
                    owner.Date = new DateTime(2015, 1, 1);
                    Globals.ctx.Owners.Add(owner);
                }
                else
                {
                    currOwner = (Owner)lvOwners.SelectedItem;
                    currOwner.Name = tbOwnerName.Text;
                    currOwner.Photo = currOwnerImage;
                }
                Globals.ctx.SaveChanges();
                ReloadRecords();  //refresh listview
                RefreshGUI();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void btOwnerManageCars_Click(object sender, RoutedEventArgs e)
        {
            ManageCarsAction();
        }

        private void lvOwners_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ManageCarsAction();
        }
        private void ManageCarsAction()
        {
            if ((Owner)lvOwners.SelectedItem == null) { return; }
            CarsDialog dlg = new CarsDialog(currOwner);
            dlg.Owner = this;
            dlg.ShowDialog();
            ReloadRecords();  //refresh listview
        }
        public void ReloadRecords()
        {
            try
            {
                lvOwners.ItemsSource = Globals.ctx.Owners.ToList();
                Utils.AutoResizeColumns(lvOwners);
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }
        private void SelectImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png|All Files (*.*)|*.*";
            if(dlg.ShowDialog() == true)
            {
                try
                {
                    currOwnerImage = File.ReadAllBytes(dlg.FileName);
                    BitmapImage bitmap = Utils.ByteArrayToBitmapImage(currOwnerImage); // ex: SystemException
                    imgSelected.Source = bitmap;
                    imgSelected.Visibility = Visibility.Visible;
                    tbImage.Visibility = Visibility.Hidden;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    MessageBox.Show(this, ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            /*
            string imagePath = "" + dlg.FileName;
            if (imagePath == "") { return; }
            currOwnerImage = File.ReadAllBytes(imagePath);
            BitmapImage bitmapImg = new BitmapImage(new Uri(imagePath));
            imgSelected.Source = bitmapImg;
            imgSelected.Visibility = Visibility.Visible;
            tbImage.Visibility = Visibility.Hidden;
            */
        }

        private void btnExportSelected_Click(object sender, RoutedEventArgs e)
        {
            var selItems = lvOwners.SelectedItems;
            if (selItems.Count == 0)
            {
                MessageBox.Show(this, "Select some items first", "Input error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            SaveFileDialog exportDialog = new SaveFileDialog();
            exportDialog.Filter = "CSV file (*.csv)|*.csv| All Files (*.*)|*.*";
            exportDialog.Title = "Export to file";
            exportDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);

            if (exportDialog.ShowDialog() == true)
            {
                try
                {
                    // FIXME: force quoting
                    var config = new CsvConfiguration(CultureInfo.InvariantCulture) { Delimiter = ";", Encoding = Encoding.UTF8, HasHeaderRecord = true };
                    // FIXME: find out how to force date format in the new csvhelper
                    // var options = new TypeConverterOptions { Formats = new[] { "yyyy-MM-dd" } };
                    //csv.Configuration.TypeConverterOptionsCache.AddOptions<DateTime>(options);
                    using (StreamWriter writer = new StreamWriter(exportDialog.FileName))
                    using (CsvWriter csv = new CsvWriter(writer, config))
                    {
                        csv.Context.RegisterClassMap<OwnerExportMap>();
                        csv.WriteRecords(selItems);
                    }
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error exporting to csv: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
        public sealed class OwnerExportMap : ClassMap<Owner>
        {
            public OwnerExportMap()
            {
                Map(m => m.Id);
                Map(m => m.Name);
                Map(m => m.CarsNumber);
                var memberMap = Map(m => m.Date);
                memberMap.TypeConverterOption.Format("yyyy-MM-dddd");
            }
        }

        /*
public void ByteToDisplay(byte[] byteVal)
{
   BitmapImage myBitmapImage = new BitmapImage();
   myBitmapImage.BeginInit();
   myBitmapImage.StreamSource = new MemoryStream(byteVal);
   myBitmapImage.EndInit();
   imgSelected.Source = myBitmapImage;
}
*/
        /*
        private void MyListView_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //method 1
             System.Windows.Point pt = e.GetPosition(this);
             System.Windows.Media.VisualTreeHelper.HitTest(this, pt);
             
            //method 2
            HitTestResult r = VisualTreeHelper.HitTest(this, e.GetPosition(this));
            if (r.VisualHit.GetType() != typeof(ListBoxItem))
                lvOwners.UnselectAll();
            
        }
        */
    }
}
