﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11CarsOwners
{
   public class Owner
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [NotMapped]
        public int CarsNumber {
            get
            {
                if(CarsInGarage == null)
                {
                    return 0;
                }
                else
                {
                    return CarsInGarage.Count();
                }
                
            }
        }
        public byte[] Photo { get; set; }
        [Required]
        public virtual ICollection<Car> CarsInGarage { get; set; }

        public override string ToString()
        {
            return $"ID:{Id} Name: {Name} NumCars: {CarsNumber}";
        }
        public  string DebugString()
        {
            return $"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<ID:{Id} Name: {Name} NumCars: {CarsNumber} CarsInGarage: {CarsInGarage.Count}>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>";
        }
        [NotMapped]
        public DateTime Date { get; set; }
    }
}
