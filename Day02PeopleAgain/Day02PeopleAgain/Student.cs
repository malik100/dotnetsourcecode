﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    class Student : Person
    {
        private string _program;
        private double _gpa;

        public Student(string name, int age, string program, double gpa)
            :base(name, age)
        {
            Program = program;
            GPA = gpa;
        }
        public string Program
        {
            get
            {
                return _program;
            }
            set
            {
                if (!Regex.IsMatch(value, @"^[^;]{1,50}$"))
                {
                    throw new InvalidParameterException("Program must be 1-50 charcters with no semi-colons");
                }
                _program = value;
            }
        }
        public double GPA
        {
            get
            {
                return _gpa;
            }
            set
            {
                if (value < 0 || value > 4.3)
                {
                    throw new InvalidParameterException("GPA must be between 0-4.3");
                }
                _gpa = value;
            }
        }
        public override string ToDataString()
        {
            return $"Student;{Name};{Age};{Program};{GPA}";
        }
        public override string ToString()
        {
            return $"Student {Name} is {Age} studying {Program} with GPA {GPA}";
        }
    }
}
