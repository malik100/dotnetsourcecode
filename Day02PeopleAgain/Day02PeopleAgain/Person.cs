﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    class Person
    {
        private string _name;
        private int _age;

        public Person(string name, int age)
        {
            Age = age;
            Name = name;
        }
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (!Regex.IsMatch(value, @"^[^;]{2,50}$"))
                {
                    throw new InvalidParameterException("Name must be 1-50 characters with no semi-colons");
                }
                _name = value;
            }
        }
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if(value < 0 || value > 150)
                {
                    throw new InvalidParameterException("Age must be between 1-150");
                }
                _age = value;
            }
        }
        public virtual string ToDataString()
        {
            return $"Person;{Name};{Age}";
        }
        public override string ToString()
        {
            return $"Person {Name} is {Age}y/o";
        }
    }
}
