﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    class Program
    {
        const string FileName = @"..\..\people.txt";
        const string OutFile = @"..\..\outputppl.txt";
        static List<Person> people = new List<Person>();
        static void Main(string[] args)
        {
            try
            {
                /*
                Person person = new Person("jerry", 40);
                Teacher teacher = new Teacher("ken", 66, "math", 3);
                Student student = new Student("Mary", 25, "Science", 3.0);
                Console.WriteLine(person);
                Console.WriteLine(teacher);
                Console.WriteLine(student);*/
                ReadDataFromFile();
                foreach (var item in people)
                {
                    Console.WriteLine(item);
                }
                Console.WriteLine("================================");
                PrintPerson("Student");
                Console.WriteLine( "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
                StudentGpa();
                WriteToDataFile();
            }
            catch (InvalidParameterException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("--------------------------------------------------\npress any key to exit program");
                Console.ReadKey();
            }

        }
        public static void WriteToDataFile()
        {
            var sortedList = people.OrderBy(x => x.Name).ToList();
            var linesList = sortedList.Select(people => people.ToDataString());
            File.WriteAllLines(OutFile, linesList);
        }

        public static void ReadDataFromFile()
        {
            try
            {
                //------------------------------------------------------------------------------
                if (!File.Exists(FileName))
                {
                    Console.WriteLine("No file found returning");
                    return;
                }
                //-------------------------------------------------------------------------------
                string[] peopleArr = File.ReadAllLines(FileName);
                //------------------ITERATING AND ADDING-----------------------------------------
                foreach (string ppl in peopleArr)
                {
                    try
                    {
                        string[] data = ppl.Split(';');
                        if (data.Length < 3 || data.Length > 5)
                        {
                            throw new FormatException("Invalid number of items");
                        }
                        InitializePerson(data);
                    }
                    catch (Exception ex) when (ex is FormatException || ex is ArgumentException || ex is InvalidParameterException)
                    {
                        //***********************************************************************
                        Debug.WriteLine(ex.ToString());
                        //***********************************************************************
                        Console.WriteLine($"Error (skipping line): {ex.Message} in:\n  {ppl}");
                    }
                }
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                //***********************************************************************
                Debug.WriteLine(ex.ToString());
                //***********************************************************************
                Console.WriteLine("Error reading file: " + ex.Message);
            }
        }
        public static void StudentGpa()
        {
            var StudentList = people.Where(x => x is Student).Cast<Student>().ToList();
            double gpaAvg = StudentList.AsQueryable().Average(x => x.GPA);
            Console.WriteLine("Average GPA was: {0}", gpaAvg);
            var SortedList = StudentList.OrderBy(x => x.GPA).ToList();
            double median;
            if(SortedList.Count % 2 == 1)
            {
                median = SortedList[SortedList.Count / 2].GPA;
                Console.WriteLine("Median is: {0:f2}", median);
            }
            else
            {
                int middleIndex = SortedList.Count / 2;
                median = (SortedList[middleIndex].GPA + SortedList[middleIndex - 1].GPA) / 2;
                Console.WriteLine("Median for even list was: {0:f2}", median);
            }
            /*
            double subSum = 0;
            for (int i = 0; i < SortedList.Count; i++)
            {
                subSum += (SortedList[i].GPA - gpaAvg) * (SortedList[i].GPA - gpaAvg);
            }
            subSum /= SortedList.Count;
            subSum = Math.Sqrt(subSum);
            Console.WriteLine("Standard deviation was {0:f2}", subSum);
            */
        }
        public static void InitializePerson(string[] data)
        {
            string check = data[0];
            switch (check)
            {
                case "Person":
                    people.Add(new Person(data[1], int.Parse(data[2])));  //FormatException
                    break;
                case "Teacher":
                    people.Add(new Teacher(data[1], int.Parse(data[2]), data[3], int.Parse(data[4])));
                    break;
                case "Student":
                    people.Add(new Student(data[1], int.Parse(data[2]), data[3], double.Parse(data[4])));
                    break;
                default:
                    return;
            }
        }

        public static void PrintPerson(string type)
        {
            switch (type)
            {
                case "Student":
                    foreach (Person stu in people)
                    {
                        if(stu.GetType() == typeof(Student))
                        Console.WriteLine(stu);
                    }
                    break;
                case "Teacher":
                    foreach (Person tea in people)
                    {
                        if(tea.GetType() == typeof(Teacher))
                        Console.WriteLine(tea);
                    }
                    break;
                case "Person":
                    foreach (Person ppl in people)
                    {
                        if(ppl.GetType() != typeof(Student) && ppl.GetType() != typeof(Teacher))
                        Console.WriteLine(ppl);
                    }
                    break;
                default:
                    return;
            }
        }
    }
}
