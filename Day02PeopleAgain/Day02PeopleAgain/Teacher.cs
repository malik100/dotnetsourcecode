﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    class Teacher : Person
    {
        private int _yearsOfExperience;
        private string _subject;

        public Teacher(string name, int age, string subject, int yearsOfExperience)
            :base(name, age)
        {
            Subject = subject;
            YearsOfExperience = yearsOfExperience;
        }
        public string Subject
        {
            get
            {
                return _subject;
            }
            set
            {
                if (!Regex.IsMatch(value, @"^[^;]{2,50}$")){
                    throw new InvalidParameterException("Subject must be 2-50 characters with no semi-colons");
                }
                _subject = value;
            }
        }
        public int YearsOfExperience
        {
            get
            {
                return _yearsOfExperience;
            }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new InvalidParameterException("Years of experience must be 0-150");
                }
                _yearsOfExperience = value;
            }
        }

        public override string ToDataString()
        {
            return $"Teacher;{Name};{Age};{Subject};{YearsOfExperience}";
        }
        public override string ToString()
        {
            return $"Teacher {Name} is {Age} teaching {Subject} with {YearsOfExperience} of experience";
        }
    }
}
