﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day08DialogPractice
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            comboStatus.ItemsSource = Enum.GetValues(typeof(EStatus)).Cast<EStatus>();
            comboStatus.SelectedIndex = 0;
        }

        private void btAddTodo_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
