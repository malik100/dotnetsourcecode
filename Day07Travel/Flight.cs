﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day07Travel
{
    public class Flight
    {
        private string _destination;
        private string _travellerName;
        private string _travellerPassport;
        private DateTime _departureDate;
        private DateTime _returnDate;

        public Flight(string destination, string name, string passport, DateTime departureDate, DateTime returnDate)
        {
            Destination = destination;
            TravellerName = name;
            TravellerPassport = passport;
            DepartureDate = departureDate;
            ReturnDate = returnDate;
        }
        public Flight(string line)
        {
            string[] data = line.Split(';');
            if (data.Length != 5)
            {
                throw new WrongDataEx("Data from file must have 5 portions separated by ';'");
            }
            CultureInfo cultureFormat = CultureInfo.InvariantCulture; //Or Current Culture
            string expectedFormat = "yyyy/M/d";
            Destination = data[0];
            TravellerName = data[1];
            TravellerPassport = data[2];
            DepartureDate = DateTime.ParseExact(data[3], expectedFormat, cultureFormat);
            ReturnDate = DateTime.ParseExact(data[4], expectedFormat, cultureFormat);
        }
        public string Destination
        {
            get { return _destination; }
            set
            {
                if (value.Length < 2 || value.Length > 30 || value.Contains(';'))
                {
                    throw new WrongDataEx("Destination must be 2 30 characters and must not contain semi-colons ");
                }
                _destination = value;
            }
        }
        public string TravellerName
        {
            get { return _travellerName; }
            set
            {
                if (value.Length < 2 || value.Length > 30 || value.Contains(';'))
                {
                    throw new WrongDataEx("Traveller name must be 2-30 characters and not contain semi-colons");
                }
                _travellerName = value;
            }
        }
        public string TravellerPassport
        {
            get { return _travellerPassport; }
            set
            {
                if (!Regex.IsMatch(value, @"^[A-Z]{2}[0-9]{6}$"))
                {
                    throw new WrongDataEx("Passport number must be two numbers followed by 6 upper case characters");
                }
                _travellerPassport = value;
            }
        }
        public DateTime DepartureDate
        {
            get { return _departureDate; }
            set
            {
                /*
                if (value < DateTime.Now)
                {
                    throw new WrongDataEx("You cannot have a departure flight scheduled in the past");
                }
                */
                _departureDate = value;
            }
        }
        public DateTime ReturnDate
        {
            get { return _returnDate; }
            set
            {
                if (value < _departureDate)
                {
                    throw new WrongDataEx("Return date can not be sooner than departure date");
                }
                _returnDate = value;
            }
        }
        public override string ToString()
        {
            return $"Flight to {Destination} Passenger: {TravellerName}, Passport: {TravellerPassport} \nLeaving {DepartureDate.ToString("yyyy/MMMM/dd")} " +
                $"\nReturning {ReturnDate.ToString("yyyy/MMMM/dd")}\n-----------------------------------------------------------------";
        }
        public string ToDataString()
        {
            return $"{Destination};{TravellerName};{TravellerPassport};" +
                $"{DepartureDate.ToString("yyyy/M/d")};{ReturnDate.ToString("yyyy/M/d")}";
        }
        //--Inavalid Data Exception CLass-----------------------------------------------------------------------------
        
    }
    public class WrongDataEx : Exception

    {
        public WrongDataEx(string msg) : base(msg) { }
    }
}
