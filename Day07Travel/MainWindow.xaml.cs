﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07Travel
{
    
    public partial class MainWindow : Window
    {
        static List<Flight> FlightsModel = new List<Flight>();
        const string InputFile = @"../../input.txt";
        const string OutputFile = @"../../output.txt";
        public MainWindow()
        {
            InitializeComponent();
            FlightsModel.Add(new Flight("Miami", "John", "AB123456", new DateTime(2000, 1, 1, 10, 30, 0), 
                new DateTime(2000, 2, 1, 10, 30, 0)));
            lstDisplay.ItemsSource = FlightsModel;
            LoadDataFromFile();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string destination = tfDestination.Text;
                string name = tfName.Text;
                string passport = tfPassport.Text;
                DateTime departureDate = (DateTime)datePickerDeparture.SelectedDate;
                DateTime returnDate = (DateTime)datePickerReturn.SelectedDate;
                FlightsModel.Add(new Flight(destination, name, passport, departureDate, returnDate));
                lstDisplay.Items.Refresh();
            }catch(WrongDataEx ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }catch(InvalidOperationException ex)
            {
                MessageBox.Show("Please fill out all fields before adding flight", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            
        }
        public static void LoadDataFromFile()
        {
            try
            {
                if (!File.Exists(InputFile))
                {
                    Console.WriteLine("file to load from not found");
                    return;
                }
                string[] linesArray = File.ReadAllLines(InputFile);
                List<string> errorList = new List<string>();
                foreach(string line in linesArray)
                {
                    try
                    {
                        FlightsModel.Add(new Flight(line));
                    }
                    catch(WrongDataEx ex)
                    {
                        errorList.Add(line + "\nREASON: " + ex.Message + "\n");
                    }
                    catch (FormatException ex)
                    {
                        errorList.Add(line + "\nREASON: " + ex.Message + "\n");
                    }
                }
                if(errorList.Count != 0)
                {
                    string errors = "";
                    foreach (var item in errorList)
                    {
                        errors += item + "\n";
                    }
                    MessageBox.Show("The following lines could not be added due to errors\n\n" + errors, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        private void SaveSelected_Click(object sender, RoutedEventArgs e)
        {
            var selItems = lstDisplay.SelectedItems.Cast<Flight>().ToList();
            if (selItems.Count == 0)
            {
                MessageBox.Show(this, "Please select some items first", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Trips files (*.trips)|*.trips|All files (*.*)|*.*";
            saveFileDialog.FilterIndex = 1;
            // saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog.ShowDialog() == true)
            {
                string fileName = saveFileDialog.FileName;
                SaveDataToFile(fileName, selItems);
            }
            /*
            SaveFileDialog save = new SaveFileDialog();
            if(save.ShowDialog() == true)
            {
                List<string> linesList = new List<string>();
                foreach (var item in FlightsModel)
                {
                    linesList.Add(item.ToDataString());
                }
                //File.WriteAllText(save.FileName, flightString + "\n");
                File.WriteAllLines(save.FileName, linesList);
                MessageBox.Show("File saved successfully", "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            */
        }
        private void SaveDataToFile(string fileName, List<Flight> flightsToSave)
        {
            try
            {
                List<string> linesList = new List<string>();
                foreach (Flight f in flightsToSave)
                {
                    linesList.Add(f.ToDataString());
                }
                File.WriteAllLines(fileName, linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show(this, ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveDataToFile(OutputFile, FlightsModel);
        }
    }
}
