﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeLINQ
{
    class Program
    {
        delegate double doubleIt(double val);
        static void Main(string[] args)
        {
            /*   ***LAMBDA with DELEGATE***-------------------------------------------------------------
            doubleIt doub = x => x + 10;
            Console.WriteLine(doub(15));
            */
            /*  ***USING WHERE FROM LISTS***------------------------------------------------------------
            List<int> numList = new List<int> { 1, 3, 5, 10, 15, 2, 8 };
            var oddList = numList.Where(x => x % 2 != 0).ToList();
            var rangeList = numList.Where(x => x >= 10).ToList();
            */
            /*--------------***USING RANDOMS AND COUNT RESULTS FROM A LIST***---------------------------
            List<int> flipList = new List<int>();
            Random ran = new Random();
            for (int i = 0; i < 100; i++)
            {
                flipList.Add(ran.Next(1, 3));
            }
            Console.WriteLine("Heads: {0}", flipList.Where(x => x ==1).ToList().Count);
            Console.WriteLine("Tails: {0}", flipList.Where(x => x == 2).ToList().Count);
            */
            /*-----------***USING WHERE ON STRINGS RETURNS IENUMERABLE OBJECT***------------------------    
            var nameList = new List<string> { "Doug", "Sally", "Sue" };
            var matchNames = nameList.Where(x => x.StartsWith("S"));
            */
            /*----------***USING SELECT ADDED 10 TO EACH VALUE***---------------------------------------
            var OneTo10 = new List<int>();
            OneTo10.AddRange(Enumerable.Range(1, 10));
            var addedNums = OneTo10.Select(x => x + 10);
            foreach(var item in addedNums)
            */
            /*---------***USING ZIP TO SUM VALUES OF TWO LISTES INTO ONE NEW LIST-----------------------
            var listOne = new List<int>(new int[] {1,2,3 });
            var listTwo = new List<int>(new int[] {4,5,6 });
            var sumList = listOne.Zip(listTwo, (x, y) => x + y);
            */
            /*------***USING AGGREGATE TO ADD TOGETHERE ALL VALUES OF THE SAME LIST***------------------
            var numListTwo = new List<int>() { 1, 2, 3, 4, 5 };
            Console.WriteLine("The combined value of the list is: {0}", numListTwo.Aggregate((a, b) => a + b));
            */
            /*-----AVERAGING VALUE IN LIST--------------------------------------------------------------
            var avgList = new List<int>() { 10, 20, 30, 40, 50 };
            Console.WriteLine("The average of the values of this list is: {0}", avgList.AsQueryable().Average());
            */
            //-------***CHECKING BOOEAN WITH ALL AND ANY KEYWORD------------------------------------------------
            /*
            var numListThree = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            Console.WriteLine("Are ALL values in this list greater than 5?: {0}", numListThree.All(x => x > 5));
            Console.WriteLine("Are ANY values in this list greater than 5?: {0}", numListThree.Any(x => x > 5));
            Console.WriteLine("Are ALL values in this list less than 15?: {0}", numListThree.All(x => x < 15));
            */
            //--DISTINCT ELIMINATES DUPLICATES FROM LIST--------------------------------------------------------
            //WITH STRING.JOIN
            //--EXCEPT COMPARES 2 LISTS
            //INTERSECT COPARES 2 LISTS RETURNS ONLY MATCHING VALUES
            Console.WriteLine("-----------------------------------------------\npress any key to exit");
            Console.ReadKey();
        }
    }
}
