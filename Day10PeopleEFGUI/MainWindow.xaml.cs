﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10PeopleEFGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        PeopleDBContext ctx;
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                ctx = new PeopleDBContext();
                lvPeople.ItemsSource = (from p in ctx.PeopleDB select p).ToList<Person>();
            }
            catch (SystemException ex) //catch all EF, SQL and other exceptions
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show(this, "Database connection failed:\n " + ex.Message, "Database error",MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1);
            }
            
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            
            string name = tbName.Text;
            if (!int.TryParse(tbAge.Text ,out int age))
            {
                MessageBox.Show(this, "Error parsing age please enter a correct age value", "Database error", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            try
            {
                ctx.PeopleDB.Add(new Person { Name = name, Age = age });
                ctx.SaveChanges();
                tbName.Text = "";
                tbAge.Text = "";
                lvPeople.ItemsSource = (from p in ctx.PeopleDB select p).ToList<Person>();
                /*
                MessageBox.Show(this, "An entry was successfully added to the database", "Database Updated", MessageBoxButton.OK, MessageBoxImage.Information);
                */
            }
            catch(SystemException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show(this, "Database connection failed:\n " + ex.Message, "Database error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            string newName = tbName.Text;
            if (!int.TryParse(tbAge.Text, out int newAge))
            {
                MessageBox.Show(this, "Error parsing age please enter a correct age value", "Database error", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            if (MessageBox.Show(this, "Permanantley update selected record?", "Update?", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {
                return;
            }
            int updateIndex = ((Person)lvPeople.SelectedItem).Id;  //get id by casting selected list item to person
            Person toUpdate = (from p in ctx.PeopleDB where p.Id == updateIndex select p).FirstOrDefault<Person>();
            if (toUpdate != null)
            {
                toUpdate.Name = newName;  // shedule name change
                toUpdate.Age = newAge; //schedult age change
                ctx.SaveChanges(); //finalize update in db
                lvPeople.ItemsSource = (from p in ctx.PeopleDB select p).ToList<Person>();
                lvPeople.SelectedIndex = -1;
            }
            else
            {
                MessageBox.Show(this, "Error Deleting person", "Update error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show(this, "Permanantley delete selected record:?\n" + lvPeople.SelectedItem, "Delete?", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {
                return;
            }
            int deletionIndex = ((Person)lvPeople.SelectedItem).Id;  //get id by casting selected list item to person
            Person toDelete = (from p in ctx.PeopleDB where p.Id == deletionIndex select p).FirstOrDefault<Person>();
            if(toDelete != null)
            {
                ctx.PeopleDB.Remove(toDelete);  // schedule delete
                ctx.SaveChanges(); //finalize deletion in db
                lvPeople.ItemsSource = (from p in ctx.PeopleDB select p).ToList<Person>();
                RefreshGUI();
            }
            else
            {
                MessageBox.Show(this, "Error Deleting person", "Deletion error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void lvPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvPeople.SelectedIndex == -1)  //if nothing selected return
            {
                RefreshGUI();
                return;
            }
            Person currPerson = (Person)lvPeople.SelectedItem;  //<<<as person or runtime error>>> "cast exception"
            if(currPerson == null) {
                RefreshGUI();
                    return;
            }
            //enable buttons and load info onto GUI
            btDelete.IsEnabled = true;
            btUpdate.IsEnabled = true;
            lblId.Content = currPerson.Id;
            tbName.Text = currPerson.Name;
            tbAge.Text = currPerson.Age.ToString();
        }

        public void RefreshGUI()
        {
            btDelete.IsEnabled = false;
            btUpdate.IsEnabled = false;
            lblId.Content = "-";
            tbName.Text = "";
            tbAge.Text = "";
        }
        
    }
}
