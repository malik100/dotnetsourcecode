using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Microsoft.AspNetCore.Authorization;
using System.IO;
using OpenMarket.Data;
// using OpenMarket.Repositories;
// using OpenMarket.Services;
// using OpenMarket.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using OpenMarket.Models;


namespace OpenMarket.Pages
{

    public class ReviewAddModel : PageModel
    {
        private readonly OpenMarketDbContext db;
       // public List<Product> ProductsList { get; set; } = new List<Product>();

        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
 
        // private IUploadRepository _uploadRepository;
        public ReviewAddModel(OpenMarketDbContext db, UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager) {
            this.db = db;
            this.userManager = userManager;
            this.signInManager = signInManager;
 
        }




        [BindProperty]
        public Review NewReview { get; set; }

        

        [BindProperty]
        public Product Product {get; set;}
        [BindProperty (SupportsGet = true)]
        public int Id { get; set; }
        [BindProperty]
        public List<Product> ProductView { get; set; } = new List<Product>();

      //  [BindProperty]
      //  public List<Product> Product { get; set; } = new List<Product>();






        public void OnGet()
        {
            ProductView = db.Products.ToList();
            Product = db.Products.Find(Id);
        }
        public async Task<IActionResult> OnPostAsync()
        {
            Product = db.Products.Find(Id);
            /*
            if (!ModelState.IsValid)
            {
                return Page();
            }
            */
          //  ProductsList = db.Products.ToList();
            //NewReview.Product = db.Products.Find(Id);
            NewReview.Product = Product;
            var user = User.Identity.Name;
            NewReview.Reviewer = db.Users.FirstOrDefault(u => u.UserName == user);   
            if(NewReview.Reviewer == null){
                NewReview.Reviewer = db.Users.FirstOrDefault(u => u.Email == "guest@guest.com");
            }
            db.Reviews.Add(NewReview);
            await db.SaveChangesAsync();

            return RedirectToPage("./ProductView", new { Id = Id});
            
        }

    }
}
