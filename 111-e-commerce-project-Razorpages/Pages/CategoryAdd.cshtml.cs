using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Microsoft.AspNetCore.Authorization;

using System.IO;
using OpenMarket.Data;
// using OpenMarket.Repositories;
// using OpenMarket.Services;
// using OpenMarket.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using OpenMarket.Models;

namespace OpenMarket.Pages
{
    [Authorize(Roles = "Admin")]
    public class CategoryAddModel : PageModel
    {
        private readonly OpenMarketDbContext db;
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;

    public CategoryAddModel(OpenMarketDbContext db, UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager) {
            this.db = db;
            this.userManager = userManager;
            this.signInManager = signInManager;
        }
        [BindProperty]
        public ProductCategory NewCategory { get; set; }
        public void OnGet()
        {
        }
                public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            var user = User.Identity.Name;

            db.ProductCategories.Add(NewCategory);
            await db.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
