using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OpenMarket.Data;
using OpenMarket.Models;

namespace OpenMarket.Pages.Admin.Transactions
{
    public class IndexModel : PageModel
    {
        private readonly OpenMarketDbContext db;
        [BindProperty]
        public List<Order> OrderList { get; set; } = new List<Order>();
        [BindProperty]
        public List<OrderDetail> OrderDetailList { get; set; } = new List<OrderDetail>();
        [BindProperty]
        public List<IdentityUser> UserList { get; set; } = new List<IdentityUser>();
        [BindProperty]
        public List<Product> ProductList { get; set; } = new List<Product>();
        public IndexModel(OpenMarketDbContext db){
            this.db = db;
        }

        public void OnGet()
        {
            OrderList = db.Orders.ToList();
            OrderDetailList = db.OrderDetails.ToList();
            UserList = db.Users.ToList();
            ProductList = db.Products.ToList();
        }
    }
}
