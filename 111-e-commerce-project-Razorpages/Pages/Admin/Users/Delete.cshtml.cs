using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
// added
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using OpenMarket.Data;
using OpenMarket.Models;

namespace OpenMarket.Pages.Admin.Users
{
    [Authorize(Roles = "Admin")]
    public class AdminDeleteModel : PageModel
    {   
        private readonly OpenMarket.Data.OpenMarketDbContext db;
        private UserManager<IdentityUser> _userManager;
        private SignInManager<IdentityUser> _signInManager;

        [BindProperty(SupportsGet = true)]
        public string Email { get; set; }
        [BindProperty]
        public string Password { get; set; }
        [BindProperty]
        public IdentityUser Useri { get; set; }

        [BindProperty(SupportsGet = true)]
        public int Id {get; set;}

        //[BindProperty]
        //public InputModel Input { get; set; }

        public AdminDeleteModel(OpenMarketDbContext db, UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            this.db = db;
            _userManager = userManager;
            _signInManager = signInManager;
        }
/*
        public class InputModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }
*/
        /// <summary>
        /// get user information
        /// </summary>
        /// <returns>no returns</returns>
        public async Task OnGet()
        {
            // var user = await _userManager.GetUserAsync(User);
            var user = db.Users.FirstOrDefault(x => x.Email == Email);

            // Email = user.Email;
            // Password = user.PasswordHash;

        }

        public async Task<IActionResult> OnPostAsync()
        { 
            // var user = await _userManager.GetUserAsync(User);
            var user =  db.Users.FirstOrDefault(x => x.Email == Email);

            if (user != null)
            {
                try {
                 // using (var transaction = db.Database.BeginTransaction()) {
                 //Gets list of Roles associated with current user
                var rolesForUser = await _userManager.GetRolesAsync(user);
                foreach (var item in rolesForUser.ToList())
                {
                    // item should be the name of the role
                    var result = await _userManager.RemoveFromRoleAsync(user, item);
                }
                await _userManager.DeleteAsync(user);
//                db.Users.Remove(user);
                await db.SaveChangesAsync();
                // }
                 return RedirectToPage("./DeleteSuccess");
                } catch (Microsoft.EntityFrameworkCore.DbUpdateException ex) { /* DO NOT IGNORE IT! */
                    return NotFound("User cannot be deleted");
                }
            }
            return Page();
            
        }

    }
}
