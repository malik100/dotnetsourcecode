using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OpenMarket.Data;
using OpenMarket.Models;
using Microsoft.AspNetCore.Authorization;

namespace OpenMarket.Pages.Admin.Category
{
[Authorize(Roles = "Admin")]
    public class IndexModel : PageModel
    {
        private readonly OpenMarket.Data.OpenMarketDbContext _context;

        public IndexModel(OpenMarket.Data.OpenMarketDbContext context)
        {
            _context = context;
        }

        public IList<ProductCategory> ProductCategory { get;set; }

        public async Task OnGetAsync()
        {
            ProductCategory = await _context.ProductCategories.ToListAsync();
        }
    }
}
