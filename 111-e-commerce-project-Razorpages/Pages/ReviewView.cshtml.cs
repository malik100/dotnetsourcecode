using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Microsoft.AspNetCore.Authorization;
using System.IO;
using OpenMarket.Data;
// using OpenMarket.Repositories;
// using OpenMarket.Services;
// using OpenMarket.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using OpenMarket.Models;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;

namespace OpenMarket.Pages
{
    public class ReviewViewModel : PageModel
    {
        private readonly OpenMarket.Data.OpenMarketDbContext db;
        private readonly ILogger<ReviewViewModel> _logger;
        
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;

        public ReviewViewModel(OpenMarketDbContext db, UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager) {
            this.db = db;
            this.userManager = userManager;
            this.signInManager = signInManager;
 
        }


        
        [BindProperty]
        public Product Product {get; set;}
        
        [BindProperty(SupportsGet = true)]
        public int Id {get; set;}
      //  [BindProperty]
        //public Review SelectedReviews {get; set;}


        [BindProperty]
        public List<Review> ReviewsList {get; set;} = new List<Review>();
        public Review ReviewView { get; set; }

          public void OnGet()
        {
          //  Review.Product = db.Review.Product.Find(Id);
            Product = db.Products.Find(Id);
            ReviewView = db.Reviews.Find(Id);  
        }
        public async Task<IActionResult> OnPostAsync()
               {
            if (!ModelState.IsValid)
            {
                return Page();
            }
          //  ProductsList = db.Products.ToList();
            ReviewView.Product = db.Products.Find(Id);
            
            var user = User.Identity.Name;
            ReviewView.Reviewer = db.Users.FirstOrDefault(u => u.UserName == user);   
            
        //    db.Reviews.Add(NewReview);
            await db.SaveChangesAsync();

            return RedirectToPage("./Index");
            
        }
            
 


    }
}
