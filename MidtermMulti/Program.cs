﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidtermMulti
{
    class Program
    {
        static List<Airport> Airports = new List<Airport>();
        const string FileName = @"..\..\data.txt";
        const string OutFile = @"..\..\out.txt";
        static void Main(string[] args)
        {
            try
            {
                ReadDataFromFile();
                while (true)
                {
                    int choice = getMenuChoice();
                    switch (choice)
                    {
                        case 1:
                            AddAirport();
                            break;
                        case 2:
                            ListAllAirports();
                            break;
                        case 3:
                            NearestAirport();
                            break;
                        case 4:
                            ElevationStandardDev();
                            break;
                        case 5:
                            //SuperFibonacci();
                            break;
                        case 6:
                            Logger.ChooseDelegateSetup();
                            break;
                        case 0:
                            WriteDataToFile();
                            return; // exit program
                        default:
                            Console.WriteLine("No such option, try again.");
                            break;
                    }
                }
            }
            finally
            {
                Console.WriteLine("-----------------------------------------------\nPress any key to exit");
                Console.ReadKey();
            }
        }
        static int getMenuChoice()
        {
            while (true)
            {
                Console.Write("What do you want to do?\n" +
                    "1.Add Airport\n" +
                    "2.List all airports info\n" +
                    "3.Find nerest airport by code\n" +
                    "4.Find Airport elevation's standard deviation\n" +
                    "5.Print nth super-Fibinachi\n" +
                    "6.Change Log delegates\n" +
                    "0.Exit\n" + "Please Choose: ");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    return choice;
                }
                Console.WriteLine("\nInvalid input, must be numerical. Try again.\n");
            }
        }
        public static void AddAirport()
        {
            try
            {
                Console.WriteLine("Enter Airport Code (3 uppercase letter)");
                string code = Console.ReadLine();
                Console.WriteLine("Enter Airport City (1-50 no semicolons)");
                string city = Console.ReadLine();
                Console.WriteLine("Enter Airport Latitude (number -90 to 90)");
                if (!double.TryParse(Console.ReadLine(), out double lat))
                {
                    throw new InvalidDataException("You must enter a number -90-90 for Latitude");
                }
                Console.WriteLine("Enter Airport Longitude (number -180 to 180)");
                if (!double.TryParse(Console.ReadLine(), out double longi))
                {
                    throw new InvalidDataException("You must enter a number -180-180 for Longitude");
                }
                Console.WriteLine("Enter Airport Elevation in metres (Whole number -1000-10000)");
                if (!int.TryParse(Console.ReadLine(), out int elevation))
                {
                    throw new InvalidDataException("You must enter a whole number no decimals -1000-10000 for elevation");
                }
                Airports.Add(new Airport(code, city, lat, longi, elevation));
                Console.WriteLine("Airport added succesfully");
            }
            catch(InvalidDataException ex)
            {
                Logger.LogFailure?.Invoke("Error reading airport data from user input");
                Console.WriteLine("Error adding airport: " + ex.Message);
            }
            
        }
        public static void ListAllAirports()
        {
            foreach(Airport air in Airports)
            {
                Console.WriteLine(air);
            }
            Console.WriteLine();
        }
        static void ReadDataFromFile()
        {
            try
            {
                //FORMAT FOR READER (code;city;latitude;longitude;elevation)
                if (!File.Exists(FileName))
                {
                    Console.WriteLine("No file to read from was found continuing program");
                    return;
                }
                string[] linesArray = File.ReadAllLines(FileName);
                foreach (string line in linesArray)
                {
                    try
                    {
                        Airports.Add(new Airport(line));
                    }
                    catch (InvalidDataException ex)
                    {
                        Logger.LogFailure?.Invoke("Error reading line from file");
                        Console.WriteLine($"Error parsing line: {ex.Message}\n >> {line}");
                    }
                }
                Console.WriteLine("Loading from file successful");
            }
            catch (IOException ex)
            {
                Logger.LogFailure?.Invoke("Error reading from file" + ex.Message);
                Console.WriteLine("Error reading file: " + ex.Message);
            }
            catch (SystemException ex)
            {
                Logger.LogFailure?.Invoke("Error reading from file" + ex.Message);
                Console.WriteLine("Error reading file: " + ex.Message);
            }
        }
        public static void ElevationStandardDev()
        {
            double ElevationSum = Airports.Sum(x => x.ElevationMetres);  //Sum of list
            double ElevationAvg = ElevationSum / Airports.Count();      //Average of list
            double ElevationSquaredSum = Airports.Sum(x => (x.ElevationMetres - ElevationAvg) //value - avg then squared and sum
                                                        * (x.ElevationMetres - ElevationAvg));
            double ElevationStdDev = Math.Sqrt(ElevationSquaredSum / Airports.Count); //squareroot of squared sum
            Console.WriteLine("The standard Deviation was {0:0.##}", ElevationStdDev);
        }
        public static void NearestAirport()
        {
            Console.Write("Please enter the code of the airport you are at:");
            string code = Console.ReadLine().ToUpper();
            //retrieving matching value from list
            List<Airport> selectedAirport = Airports.Where(x => x.Code == code).ToList();
            Console.WriteLine(selectedAirport.Count);
            if (Airports.Count< 3 || selectedAirport.Count < 1)
            {
                Console.WriteLine("Not enough airports in list to match or invalid code entered.");
                return;
            }
            Airport firstAirport = selectedAirport[0];
            Console.WriteLine("You chose:" + firstAirport + "\n");
            double shortestDist = double.MaxValue;  //max value of double to compare each object value
            Airport matched = null;  //null variable to hold matched airport
            var firstAirportCoord = new GeoCoordinate(firstAirport.Latitude, firstAirport.Longitude);
            foreach (Airport search in Airports)
            {
                var secondAirportCoord = new GeoCoordinate(search.Latitude, search.Longitude);
                double currentDistance = firstAirportCoord.GetDistanceTo(secondAirportCoord);
                if (currentDistance < shortestDist && search.Code != firstAirport.Code)
                {
                    shortestDist = currentDistance;  //new shortest value
                    matched = search;                //airport with the matched shortest value
                }
            }
            Console.WriteLine("Found nearest airport {0} with distance: {1:f2} KM", matched.City, (shortestDist / 1000));
            
        }
        static void WriteDataToFile()
        {
            try
            {
                List<string> linesList = new List<string>();
                foreach (Airport air in Airports)
                {
                    linesList.Add(air.ToDataString());
                }
                File.WriteAllLines(FileName, linesList); // ex IOException, SystemException
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                Logger.LogFailure?.Invoke("Error Writing to file " + ex.Message);
                Console.WriteLine("Error writing file: " + ex.Message);
            }
        }

    }
}
