﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MidtermMulti
{
    class Airport
    {
		private string _code; // exactly 3 uppercase letters, use regexp
		private string _city; // 1-50 characters, no semicolons
		private double _latitude;  // -90 to 90, -180 to 180
	    private double _longitude;
		int _elevationMeters;// -1000 to 10000

        public Airport(string code, string city, double lat, double lng, int elevM)
        {
            Code = code;
            City = city;
            Latitude = lat;
            Longitude = lng;
            ElevationMetres = elevM;
        }
        public Airport(string line) {
            //FORMAT FOR READER (code;city;latitude;longitude;elevation)
            string[] dataLine = line.Split(';');
            if (dataLine.Length != 5)
            {
                throw new InvalidDataException("Data from file must contain 5 lines");
            }
            Code = dataLine[0];
            City = dataLine[1];
            if (!double.TryParse(dataLine[2], out double lat))
            {
                throw new InvalidDataException("File to read from have -90 to 90 for Latitude");
            }
            Latitude = lat;
            if (!double.TryParse(dataLine[3], out double longi))
            {
                throw new InvalidDataException("File to read from have -180 to 180 for Longitude");
            }
            Longitude = longi;
            if (!int.TryParse(dataLine[4], out int elevation))
            {
                throw new InvalidDataException("File to read from must have+" +
                    " a whole number no decimals -1000-10000 for elevation");
            }
            ElevationMetres = elevation;
        }


        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
                {
                    throw new InvalidDataException("Airport name must be 1-50 characters with no semicolon");
                }
                _city = value;
            }
        }
        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                if (!Regex.IsMatch(value, @"^[A-Z]{3}$"))
                {
                    throw new InvalidDataException("Airport code must be 3 upper case letters.");
                }
                _code = value;
            }
        }
        public double Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                if (value < -90 || value > 90)
                {
                    throw new InvalidDataException("Latitude must be -90 to 90.");
                }
                _latitude = value;
            }
        }
        public double Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                if (value < -180 || value > 180)
                {
                    throw new InvalidDataException("Longitude must be -180 to 180.");
                }
                _longitude = value;
            }
        }
        public int ElevationMetres
        {
            get
            {
                return _elevationMeters;
            }
            set
            {
                if (value < -1000 || value > 10000)
                {
                    throw new InvalidDataException("Longitude must be -1000 to 10000.");
                }
                _elevationMeters = value;
            }
        }
        public override string ToString()
        {
            return $"Airport {Code} in {City} Longitude: {Longitude}, Latitude: {Latitude} Elevation: {ElevationMetres}";
        }
        
        public string ToDataString()
        {
            return $"{Code};{City};{Latitude};{Longitude};{ElevationMetres}";
        }
    }
}
