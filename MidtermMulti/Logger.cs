﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidtermMulti
{
    class Logger
    {
        public static int LogNumber = 1; //static field to hold log #
        public delegate void LogFailedSetterDelegate(string reason);

        public static LogFailedSetterDelegate LogFailure;

        public static void LogToFile(string msg)
        {
            try
            {
                File.AppendAllText(@"..\..\log.txt", LogNumber + ": " + msg + "\n");
                LogNumber++;
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                Console.WriteLine("Error saving log message to file: " + ex.Message);
            }
        }

        public static void LogToConsole(string msg)
        {
            Console.WriteLine("*** LOG: " + msg);
        }

        public static void ChooseDelegateSetup()
        {
            LogFailure = LogToConsole;
            Console.Write("Where would you like to log setters errors?\n"
                + "1 - Logging to console\n2 - Logging to file\nYour choice: ");
            string choice = Console.ReadLine();  // getting user choice
            string[] choiceArr = choice.Split(','); // splitting into arryay with ',' delimiter
            //looping throgh choice array
            try {
                for (int i = 0; i < choiceArr.Length; i++)
                {
                    if (!int.TryParse(choiceArr[i], out int key))
                    {
                        throw new InvalidDataException("Error unexpected key typed");
                    }
                    if (key == 1)
                    {
                        Console.WriteLine("Logging to Console Enabled");
                        LogFailure += LogToConsole;
                    }
                    if (key == 2)
                    {
                        Console.WriteLine("Logging to File Enabled");
                        LogFailure += LogToFile;
                    }
                }
            }
            catch (InvalidDataException)
            {
                Console.WriteLine("Invalid entry switching to default Logging disabled");
                LogFailure = null;
            }
        }
    }
}
