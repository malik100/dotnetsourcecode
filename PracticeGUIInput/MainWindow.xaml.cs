﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PracticeGUIInput
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static List<string> Continents = new List<string>{ "North America", "Soth America", "Asia", "Africa", 
                                                            "Antartica", "Antartica", "Europe"};
        public MainWindow()
        {
            InitializeComponent();
            comboContinent.ItemsSource = Continents;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("You clicked the button");
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void CheckBox_Checked_1(object sender, RoutedEventArgs e)
        {

        }

        private void RegisterMeButton_Click_1(object sender, RoutedEventArgs e)
        {
            string Name = tfName.Text;
            string ageStr = "";
            string continentStr = (string)comboContinent.SelectedItem;
            string sliderVal = Convert.ToString(Slider.Value);
            if (radio18to35.IsChecked == true)
            {
                ageStr = (string)radio18to35.Content;
            }
            else if(radio36andup.IsChecked == true){
                ageStr = (string)radio36andup.Content;
            }else if (radioBelow18.IsChecked == true)
            {
                ageStr = (string)radioBelow18.Content;
            }
            else
            {
                MessageBox.Show("Please pick an age option", "Error Selection", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            MessageBox.Show("Name: " + Name + " " + "Age: " +  ageStr + " Continent: " +
                continentStr + " Slider Value: " + sliderVal, "Title", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
