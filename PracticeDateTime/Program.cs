﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeDateTime
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var date = new DateTime(2008, 5, 1, 8, 30, 52);
                date = date.AddSeconds(3);
                Console.WriteLine(date.ToString("yyyy-MM-dd hh:mm:ss tt")); //HH 24hour clock
                Console.WriteLine("----------------------------------------------------------");
                string dateStr = "2000/01/01";
                var dateTwo = DateTime.Parse(dateStr);
                Console.WriteLine(dateTwo);
            }
            finally
            {
                Console.WriteLine("---------------------------------\nPress any key to exit program");
                Console.ReadKey();
            }
            
        }
    }
}
