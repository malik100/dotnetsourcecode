﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeFileIO
{
    class Program
    {
        const string OutFile = "../../output.txt";
        const string InFile = "../../input.txt";
        const string Directory = "../../";
        const string StreamTest = "../../streamtest.txt";
        const string strReadWri = "../../streamWriter.txt";
        const string TypePath = "../../types.dat";
        static void Main(string[] args)
        {
            string[] Animals = { "Giraffe", "Zebra", "Crocodile" };
            /***********WRITING / READING BASIC USING FILE CLASS************************************************
            File.WriteAllLines(OutFile, Animals);
            foreach (string s in File.ReadAllLines(InFile))
            {
                Console.WriteLine(s);
            }
            */
            /**************LOOKING FOR FILES IN DIRECTORY USING EXTENSIONS**************************************
            DirectoryInfo dir = new DirectoryInfo(Directory);
            FileInfo[] txtFilesAva = dir.GetFiles("*.txt", SearchOption.TopDirectoryOnly);
            foreach (var item in txtFilesAva)
            {
                Console.WriteLine(item.Name);
            }
            */
            /**********READING AND WRITING USING FILESTREAM*****************************************************
            FileStream fs = File.Open(StreamTest, FileMode.Create);
            string randString = "This is a random string";
            byte[] rsByteArray = Encoding.Default.GetBytes(randString);
            fs.Write(rsByteArray, 0, rsByteArray.Length);
            fs.Position = 0;
            byte[] recieveBytes = new byte[rsByteArray.Length];
            for (int i = 0; i < rsByteArray.Length; i++)
            {
                recieveBytes[i] = (byte)fs.ReadByte();
            }
            Console.WriteLine(Encoding.Default.GetString(recieveBytes));
            fs.Close();
            */
            /************************USING STREAM WRITER AND READER**********************************************
            StreamWriter sw = File.CreateText(strReadWri);
            sw.Write("This is a random ");
            sw.WriteLine("sentence");
            sw.WriteLine("This is another sentence");
            sw.Close();
            StreamReader sr = File.OpenText(strReadWri);
            Console.WriteLine("Peek : {0}", Convert.ToChar(sr.Peek()));
            Console.WriteLine("\n\nFirst String : {0}", sr.ReadLine());
            Console.WriteLine("\n\nRead Entire File: {0}", sr.ReadToEnd());
            sr.Close();
            */
            /************************READING WRITING USING BINARY READER / WRITER********************************
            FileInfo datFile = new FileInfo(TypePath);
            BinaryWriter bw = new BinaryWriter(datFile.OpenWrite());
            string binaryText = "Random Text";
            int myAge = 50;
            double height = 10.50;
            bw.Write(binaryText);
            bw.Write(myAge);
            bw.Write(height);
            bw.Close();
            BinaryReader br = new BinaryReader(datFile.OpenRead());
            Console.WriteLine(br.ReadString());
            Console.WriteLine(br.ReadInt32());
            Console.WriteLine(br.ReadDouble());
            br.Close();
            */
            Console.WriteLine("----------------------------------------------\nPress any key to exit program");
            Console.ReadKey();
        }
    }
}
