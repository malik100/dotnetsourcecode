﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
//using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFPicturesGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            tbImage.Visibility = Visibility.Visible;
            imgPreview.Visibility = Visibility.Hidden;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            OpenFileDialog dlg = new OpenFileDialog();
            /*
            dlg.ShowDialog();
            string imagePath = dlg.FileName;
            Image img = Image.FromFile(imagePath);
            byte[] imgBytes = imageToByteArray(img);
            BitmapImage bitmap = ImageFromBuffer(imgBytes);
            */
            dlg.ShowDialog();
            string imagePath = "" +  dlg.FileName;
            if (imagePath == "") { return; }
            byte[] currImage = File.ReadAllBytes(imagePath);
            BitmapImage bitmapImg = new BitmapImage(new Uri(imagePath));//ImageFromBuffer(currImage);
            imgPreview.Source = bitmapImg;
            imgPreview.Visibility = Visibility.Visible;
            tbImage.Visibility = Visibility.Hidden;
        }

        public byte[] imageToByteArray(Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, ImageFormat.Png);
            return ms.ToArray();
        }

        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        public BitmapImage ImageFromBuffer(Byte[] bytes)
        {
            MemoryStream stream = new MemoryStream(bytes);
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = stream;
            image.EndInit();
            return image;
        }
    }
}
/*
            // create an Image object from File
            Image image = Image.FromFile(@"D:\test\bridge.jpg");
            // create a MemoryStream 
            var ms = new MemoryStream();  // this is where we are going to deposit the bytes
            // save bytes to ms
            image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            // to get the bytes we type
            var bytes = ms.ToArray();
            // we can now save the byte array to a db, file, or transport (stream) it.
            */
