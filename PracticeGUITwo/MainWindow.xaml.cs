﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PracticeGUITwo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btApply_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show($"The description is: {this.tfDescription.Text}");
        }

        private void btReset_Click(object sender, RoutedEventArgs e)
        {
            this.cbContent.IsChecked = this.cbDrill.IsChecked = this.cbLaser.IsChecked = this.cbLathe.IsChecked =
                this.cbFold.IsChecked = this.cbPlasma.IsChecked = this.cbPurchase.IsChecked =
                this.cbRoll.IsChecked = this.cbSaw.IsChecked = this.cbWeld.IsChecked = false;
        }

        private void cb_Checked(object sender, RoutedEventArgs e)
        {
            this.tfLength.Text += (string)((CheckBox)sender).Content + " ";
        }

        private void comboBoxFinish_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(this.tfNote == null)
            {
                return;
            }
            var Combo = (ComboBox)sender;
            var ComboItem = (ComboBoxItem)Combo.SelectedValue;
            this.tfNote.Text = (string)ComboItem.Content;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            tfMass.Text = (string)((TextBox)sender).Text;
        }
    }
}
