﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06IceCreamSelector
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    
    public partial class MainWindow : Window
    {
        static List<string> LeftList = new List<string> { "Vanilla", "Chocolate", "Strawberry", "Peach" };
        static List<string> RightList = new List<string>();
        
        public MainWindow()
        {
            InitializeComponent();
            LeftPanel.ItemsSource = LeftList;
            RightPanel.ItemsSource = RightList;
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            if (LeftPanel.SelectedItem == null)
            {
                MessageBox.Show("Select an item from the left list to add","Error", 
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            string flav = (string)LeftPanel.SelectedItem;
            RightList.Add(flav);
            RightPanel.Items.Refresh();
            LeftPanel.Items.Refresh();
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            if(RightPanel.SelectedItem == null)
            {
                MessageBox.Show("Select a scoop from the right box before deleting it", "Error", 
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            int index = RightPanel.SelectedIndex;
            RightList.RemoveAt(index);
            RightPanel.Items.Refresh();
        }
    }
}
