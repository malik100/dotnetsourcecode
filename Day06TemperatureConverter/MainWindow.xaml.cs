﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06TemperatureConverter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if(rbCelIn == null || tfInput.Text == "")
            {
                return;
            }
            double cel;
            double inVal = double.Parse(tfInput.Text);
            /*
            if (double.TryParse(tfInput.Text ,out double inVal))
            {
                MessageBox.Show("Input Error");
                return;
            }
            */
            if (rbCelIn.IsChecked == true)
            {
                cel = inVal;
            }
            else if (rbFahIn.IsChecked == true)
            {
                cel = (inVal - 32) * 5 / 9;
            }
            else if (rbKelIn.IsChecked == true)
            {
                cel = inVal - 273.15;
            }
            else
            {  //should not happen
                MessageBox.Show("An internal error happened", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            //-------------------------------------------------------------------------------------------------------
            double outVal;
            String unit;
            if (rbCelOut == null)
            {
                return;
            }
            if (rbCelOut.IsChecked == true)
            {
                outVal = cel;
                unit = "C";
            }
            else if (rbFahOut.IsChecked == true)
            {
                outVal = cel * 9 / 5 + 32;
                unit = "F";
            }
            else if (rbKelOut.IsChecked == true)
            {
                outVal = cel + 273.15;
                unit = "K";
            }
            else
            {  //should not happen
                MessageBox.Show("An internal error happened", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            //4 display the result
            String outStr = String.Format("{0:f2}{1}", outVal, unit);
            tfOutput.Text = outStr;

        }
    }
}
