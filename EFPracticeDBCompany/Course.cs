﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFPracticeDBCompany
{
    public class Course
    {
        public int Id {get; set;}
        public string ClassName { get; set; }
        public virtual ICollection<Student> Enrolees { get; set; }
        public override string ToString()
        {
            return $"{Id}:{ClassName}";
        }
    }
}
