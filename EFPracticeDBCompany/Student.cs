﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFPracticeDBCompany
{
    public class Student
    {
        public int Id { get; set; }
        public string StudentName { get; set; }
        public virtual ICollection<Course> Classes { get; set; }

        public override string ToString()
        {
            return $"{Id}:{StudentName}";
        }
    }
}
