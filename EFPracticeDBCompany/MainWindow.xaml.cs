﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;

namespace EFPracticeDBCompany
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SchoolContextDB ctx;
        public MainWindow()
        {
            InitializeComponent();
            ctx = new SchoolContextDB();
            /*
            Student Jake = new Student { StudentName = "Jake", Classes = new List<Course>()};
            Student Mary = new Student { StudentName = "Mary", Classes = new List<Course>() };
            Student Lou = new Student { StudentName = "Lou", Classes = new List<Course>() };
            Student Jimmy = new Student { StudentName = "Jimmy", Classes = new List<Course>() };
            Student Larry = new Student { StudentName = "Larry", Classes = new List<Course>() };

            Course Math = new Course { ClassName = "Math", Enrolees = new List<Student>() };
            Course English = new Course { ClassName = "English", Enrolees = new List<Student>() };
            Course Gym = new Course { ClassName = "Gym", Enrolees = new List<Student>() };
            ctx.StudentTab.Add(Jake);
            ctx.StudentTab.Add(Mary);
            ctx.StudentTab.Add(Lou);
            ctx.StudentTab.Add(Jimmy);
            ctx.StudentTab.Add(Larry);

            ctx.ClassTab.Add(Math);
            ctx.ClassTab.Add(English);
            ctx.ClassTab.Add(Gym);

            Math.Enrolees.Add(Larry);
            Math.Enrolees.Add(Mary);

            English.Enrolees.Add(Larry);
            English.Enrolees.Add(Mary);
            English.Enrolees.Add(Jake);
            English.Enrolees.Add(Lou);
            English.Enrolees.Add(Jimmy);

            Gym.Enrolees.Add(Mary);
            Gym.Enrolees.Add(Jimmy);
            */
            List<Student> studentList = ctx.StudentTab.ToList();
            lvStudentsOnly.ItemsSource = studentList;
            List<Course> courseList = ctx.ClassTab.ToList();
            lvCourses.ItemsSource = courseList;
            var allStudents = ctx.StudentTab.Include(s => s.Classes);
            //lvStudentCourses.ItemsSource = allStudents.ToList();
            lvStudentCourses.ItemsSource = (from student in ctx.StudentTab
                                           from course in ctx.ClassTab
                                            where course.Enrolees.Contains(student)
                                          select new { StudentName = student.StudentName, ClassName = course.ClassName }).ToList();
            foreach(Student s in studentList)
            {
                foreach (Course c in s.Classes)
                {
                    Console.WriteLine(c);
                }
                Console.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            }
            ctx.SaveChanges();
            
        }
    }
}
