﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamplePrograms
{
    class Animal
    {
        private string _name;
        private string _sound;

        public Animal()
            : this("No Name", "No Sound") { }
        //public Animal(string name)
        //: this(name, "No Sound") { }
        public Animal(string name, string sound)
        {
            Name = name;
            Sound = sound;
        }

        //Properties----------------------------------------------------------------------------------
        public string Name
        {
            get{ return _name; }
            set{
                if(value.Length < 2 || value.Length > 30)
                {
                    throw new ArgumentException("Name must be between 2-30 characters");
                }
                _name = value; 
            }
        }
        public string Sound
        {
            get { return _sound; }
            set {
                if(value.Length < 2 || value.Length > 30)
                {
                    throw new ArgumentException("Sound must be between 1-30 characters");
                }
                _sound = value; 
            }
        }
        //ToString-------------------------------------------------------------------------
        public override string ToString()
        {
            return $"{_name} says {_sound}";
        }
    }
}
