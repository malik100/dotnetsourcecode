﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamplePrograms
{
    class PrimeArray
    {
        public bool this[long index]
        {
            get
            {
                return isPrime(index);
            }
        }

        private bool isPrime(long num)
        {
            for (int div = 2; div <= num / 2; div++)
            {
                if(num % div == 0)
                {
                    return false;
                }
                
            }
            return true;
        }
    }
}
