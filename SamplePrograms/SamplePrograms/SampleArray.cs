﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamplePrograms
{
    
    class SampleArray
    {
        private List<int> arr = new List<int>();
        public SampleArray()
        {

        }
        public bool this[int index]
        {
            
            get
            {
                for (int div = 2; div <= index/2; div++)
                {
                    if (index % div == 0)
                    {
                        return false;
                    }
                    
                }
                return true;
            }
        }
    }
}
