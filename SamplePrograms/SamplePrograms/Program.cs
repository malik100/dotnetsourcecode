﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
namespace SamplePrograms
{
    class Program
    {
        delegate double doubleIt(double val);
        static void Main(string[] args)
        {
            
            try
            {
                /*
                PrimeArray p = new PrimeArray();
                for (int i = 5; i < 50; i++)
                {
                    bool result = p[i];
                    Console.WriteLine("{0} is a prime number : {1}", i, result);
                }
                */
                SampleArray sample = new SampleArray();
                List<int> primeNumbers = new List<int>();
                int x = 20;
                for (int i = 2; i < int.MaxValue; i++)
                {
                    if (sample[i])
                    {
                        primeNumbers.Add(i);
                        if(primeNumbers.Count == x)
                        {
                            break;
                        }
                    }
                }
                foreach (var item in primeNumbers) 
                {
                    Console.WriteLine(item);
                }
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("----------------------------------------------------------------------\nPress any key to exit");
                Console.ReadKey();  
            }
            
        }

        public delegate void dele(int num1, int num2);
        
        static void doubNum(int x, out int results)
        {
            results = x * 2;
        }
    }
}
