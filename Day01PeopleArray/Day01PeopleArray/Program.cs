﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01PeopleArray
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string[] namesArray = new string[4];
                int[] agesArray = new int[4];
                for (int i = 0; i < 4; i++)
                {
                    Console.WriteLine("Enter name #{0}", i + 1);
                    string name = Console.ReadLine();
                    namesArray[i] = name;
                    Console.WriteLine("Enter age #{0}", i + 1);
                    int age;
                    if (!int.TryParse(Console.ReadLine(), out age))
                    {
                        Console.WriteLine("Error parsing age");
                        return;//Environment.Exit(1);
                    }
                    
                    agesArray[i] = age;
                }
                //print them back
                Console.Write("The names you entered: ");
                for (int i = 0; i < namesArray.Length; i++)
                {
                    Console.Write("{0}{1}({2}y/o)", i == 0 ? "" : ", ", namesArray[i], agesArray[i]);
                }
            }
            finally
            {
                Console.WriteLine();
                Console.ReadKey();
            }
            
        }
    }
}
