﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10FirstEF
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SocietyDbContext ctx = new SocietyDbContext();
                Random random = new Random();
                // equivalent of insert
                Person p1 = new Person { Name = "Jerry" + random.Next(100), Age = random.Next(100) };
                ctx.People.Add(p1); // insert operation is scheduled but NOT executed yet
                ctx.SaveChanges();
                Console.WriteLine("record inserted");
                //UPDATE: equivalent of update - fetch, modify, save changes
                // FirstOrDefault will either return Person or null
                Person p2 = (from p in ctx.People where p.PersonId == 2 select p).FirstOrDefault<Person>();
                if(p2 != null)
                {
                    p2.Name = "Ken" + (random.Next(10000) + 1000); //entity framework will know value is chaged
                    ctx.SaveChanges(); //update the database to synchronize entities in memory with the database
                    Console.WriteLine("Record updated");
                }
                else{
                    Console.WriteLine("Record to update not found");
                }
                //delete - fetch then schedule for deletion then save changes
                Person p3 = (from p in ctx.People where p.PersonId == 2 select p).FirstOrDefault<Person>();
                if (p3 != null)
                {
                    ctx.People.Remove(p3); //schedule for deletion in the databse
                    ctx.SaveChanges(); //update the database to synchronize entities in memory with the database
                    Console.WriteLine("Record deleted");
                }
                else
                {
                    Console.WriteLine("Record to delete not found");
                }
            }
            catch (SystemException ex)
            {
                Console.WriteLine("Database operation failed: " + ex.Message);
            }
            finally
            {
                Console.WriteLine("----------------------------------------\nAny key to exit");
                Console.ReadKey();
            }
        }

        private static Random Random()
        {
            throw new NotImplementedException();
        }
    }
}
