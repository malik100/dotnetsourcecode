﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day08TodoGUI
{

   
    class Todo
    {
        string _task;
        int _difficulty;
        DateTime _dueDate;
        Status _status;

        public Todo(string task, int difficulty, DateTime dueDate, Status status)
        {
            Task = task;
            Difficulty = difficulty;
            DueDate = dueDate;
            Status = status;
        }

        public string Task
        {
            get { return _task; }
            set
            {
                if (!Regex.IsMatch(value, @"^[a-zA-Z0-9 _%(),\/\\\.-]{1,100}$"))
                {
                    throw new WrongDataEx("Task must be 1-100 letters with %_-(),./\\ characters only");
                }
                _task = value;
            }
            
        }
        public int Difficulty
        {
            get { return _difficulty; }
            set
            {
                if (value < 1 || value > 5)
                {
                    throw new WrongDataEx("Difficulty must be between 1-5");
                }
                _difficulty = value;
            }
        }

        public DateTime DueDate
        {
            get { return _dueDate; }
            set
            {
                if (value < DateTime.Now)
                {
                    throw new WrongDataEx("Due date can not be in the past");
                }
                _dueDate = value;
            }
        }
        // ReadOnly DueDate------------------------------------
        public string FormatedDueDate
        {
            get
            {
                return _dueDate.ToString("d");
            }
        }
        public Status Status
        {
            get { return _status; }
            set
            {
                _status = value;
            }
        }
        public override string ToString()
        {
            return $"{Task} by {DueDate} (Difficulty: {Difficulty}, {Status})";
        }
        
    }

    //----------CUSTOM EXCEPTION CLASS----------------------------------------------------
    public class WrongDataEx : Exception

    {
        public WrongDataEx(string msg) : base(msg) { }
    }
}
