﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day08TodoGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {

        static List<Todo> TodoList = new List<Todo>();
        static List<Status> StatusList = new List<Status>();
        public MainWindow()
        {

            InitializeComponent();
            TodoList.Add(new Todo("Eat", 1, new DateTime(2021, 10, 1), Status.Pending));
            TodoDisplay.ItemsSource = TodoList;
            comboStatus.ItemsSource = new List<Status>();
            StatusList.Add(Status.Pending);
            StatusList.Add(Status.Delegated);
            StatusList.Add(Status.Done);
            comboStatus.ItemsSource = StatusList;
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string task = tfTask.Text;
                int difficulty = (int)sliderDifficulty.Value;
                if (dpDueDate.SelectedDate == null)
                {
                    MessageBox.Show("Select a date before adding todo", "Error");
                    return;
                }
                DateTime dueDate = (DateTime)dpDueDate.SelectedDate;
                Status status = (Status)comboStatus.SelectedItem;
                TodoList.Add(new Todo(task, difficulty, dueDate, status));
                TodoDisplay.Items.Refresh();
            }
            catch (WrongDataEx ex)
            {
                MessageBox.Show(ex.Message, "Error");
                return;
            }
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            DeleteAction(TodoDisplay);
        }

        private void ListViewItem_MouseDoubleClick(object sender, RoutedEventArgs e)
        {
            Todo currTodo = (Todo)TodoDisplay.SelectedItem;
            tfTask.Text = currTodo.Task;
            sliderDifficulty.Value = currTodo.Difficulty;
            dpDueDate.SelectedDate = currTodo.DueDate;
            comboStatus.SelectedItem = currTodo.Status;
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            /*
            Todo editedTodo = TodoList[TodoDisplay.SelectedIndex];
            editedTodo.Task = tfTask.Text;
            editedTodo.Difficulty = (int)sliderDifficulty.Value;
            editedTodo.DueDate = (DateTime)dpDueDate.SelectedDate;
            editedTodo.Status = (Status)comboStatus.SelectedItem;
            TodoDisplay.Items.Refresh();
            */
            UpdateAction(TodoDisplay, tfTask, sliderDifficulty, dpDueDate, comboStatus);
        }

        private void miDelete_Click(object sender, RoutedEventArgs e)
        {
            DeleteAction(TodoDisplay);
        }

        public static void DeleteAction(ListView TodoDisplay)
        {
            int[] indexesArray = new int[TodoDisplay.SelectedItems.Count];

            foreach (var item in TodoDisplay.SelectedItems)
            {
                TodoList.RemoveAt(TodoList.IndexOf((Todo)item));
            }
            TodoDisplay.SelectedItems.Clear();
            TodoDisplay.Items.Refresh();
        }

        public static void UpdateAction(ListView TodoDisplay, TextBox tfTask, Slider sliderDifficulty,
            DatePicker dpDueDate, ComboBox comboStatus)
        {
            Todo editedTodo = TodoList[TodoDisplay.SelectedIndex];
            editedTodo.Task = tfTask.Text;
            editedTodo.Difficulty = (int)sliderDifficulty.Value;
            editedTodo.DueDate = (DateTime)dpDueDate.SelectedDate;
            editedTodo.Status = (Status)comboStatus.SelectedItem;
            TodoDisplay.Items.Refresh();
        }
        

        private void miUpdate_Click(object sender, RoutedEventArgs e)
        {
            UpdateAction(TodoDisplay, tfTask, sliderDifficulty, dpDueDate, comboStatus);
        }
    }
}
